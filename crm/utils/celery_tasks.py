from django.conf import settings
from django.core.mail import send_mail

from crm.celery import app


@app.task
def send_password(email: str, password: str):
    message = f"""
    Мы рады приветствовать Вас в нашей команде!
    Используйте данные ниже для входа в систему:
    Ваш логин: {email}
    Ваш пароль: {password}
    """
    send_mail(
        "Добро пожаловать в 50 PROGRAMMERS!",
        message,
        settings.EMAIL_HOST_USER,
        ["nursultandev@gmail.com"],
        fail_silently=False,
    )
