from apps.users import views
from django.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("", views.UserViewSet, basename="users")

urlpatterns = [
    path("students/", include("apps.students.urls")),
    path("trainers/", include("apps.trainers.urls")),
    path("administrators/", include("apps.administrators.urls")),
    path("current-user/", views.current_user, name="current_user"),
]

urlpatterns += router.urls
