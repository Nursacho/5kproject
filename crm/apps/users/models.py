import datetime

from apps.users import choices
from apps.users.managers import CustomUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from utils import generators
from utils.validators import phone_number_regex


class Users(AbstractUser):
    username = None
    email = models.EmailField(unique=True, verbose_name="Основной Email")
    is_trainer = models.BooleanField(default=False, verbose_name="Тренер")
    is_student = models.BooleanField(default=False, verbose_name="Студент")
    is_administrator = models.BooleanField(default=False, verbose_name="Администратор")
    phone_number = models.CharField(
        max_length=13,
        validators=[phone_number_regex],
        blank=True,
        null=True,
        verbose_name="Номер телефона",
    )
    second_phone_number = models.CharField(
        max_length=13,
        validators=[phone_number_regex],
        blank=True,
        null=True,
        verbose_name="Резервный номер телефона",
    )
    birth_date = models.DateField(
        default=datetime.date.today, verbose_name="Дата рождения"
    )
    gender = models.CharField(
        max_length=10, choices=choices.GENDER_CHOICES, verbose_name="Пол"
    )
    avatar = models.ImageField(
        upload_to=generators.generate_image_filename,
        verbose_name="Фото профиля",
        null=True,
        blank=True,
    )

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"
        ordering = ("-id",)

    def __str__(self):
        return self.email
