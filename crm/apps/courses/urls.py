from apps.courses import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("coursestags", views.CourseTagViewSet, basename="course_tags")
router.register("", views.CourseViewSet, basename="courses")

urlpatterns = router.urls
