from apps.courses.models import Course
from apps.courses.models import CoursesTag
from apps.lessons.serializers import LessonSerializer
from apps.homeworks.serializers import HomeworkSerializer
from rest_framework import serializers


class CourseTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoursesTag
        fields = ("id", "title")


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = "__all__"


class CourseListSerializer(serializers.ModelSerializer):
    tags = CourseTagSerializer(many=True, read_only=True)
    lessons = LessonSerializer(many=True, read_only=True)
    homeworks = HomeworkSerializer(many=True, read_only=True)

    class Meta:
        model = Course
        fields = "__all__"
