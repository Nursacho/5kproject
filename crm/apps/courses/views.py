from apps.courses.models import Course
from apps.courses.models import CoursesTag
from apps.courses.serializers import CourseListSerializer
from apps.courses.serializers import CourseSerializer
from apps.courses.serializers import CourseTagSerializer
from rest_framework import viewsets


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return CourseListSerializer
        return self.serializer_class


class CourseTagViewSet(viewsets.ModelViewSet):
    queryset = CoursesTag.objects.all()
    serializer_class = CourseTagSerializer
