from django.db import models
from apps.courses.models import Course
from apps.students.models import Student
from django.db.models.signals import post_save
from django.dispatch import receiver


class Attendance(models.Model):
    student = models.ForeignKey(
        Student,
        on_delete=models.CASCADE,
        verbose_name='Студетн',
        related_name='attendances',
    )
    status = models.BooleanField(
        default=False,
        verbose_name='Посещение'
    )
    schedule = models.ForeignKey(
        'Schedule',
        on_delete=models.CASCADE,
        verbose_name='День',
        related_name='schedule_attendances'
    )

    class Meta:
        verbose_name = 'ПОСЕЩАЕМОСТЬ'
        verbose_name_plural = 'ПОСЕЩАЕМОСТИ'
        ordering = ('-id',)

    def __str__(self):
        return f"{self.student.user.email} | {self.status}"


class Schedule(models.Model):
    course = models.ForeignKey(
        Course,
        related_name='schedules',
        verbose_name='Курс',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )
    day = models.DateField(verbose_name='Дата события')
    start_time = models.TimeField(verbose_name='Время начало урока')
    end_time = models.TimeField(verbose_name='Время до конца урока')

    def __str__(self):
        return str(self.day)

    class Meta:
        verbose_name = 'ГРАФИК'
        verbose_name_plural = 'ГРАФИКИ'
        ordering = ('-id',)


@receiver(post_save, sender=Schedule)
def create_attendance(sender, instance, created, *args, **kwargs):
    if created:
        students = instance.course.students.all()
        for student in students:
            Attendance.objects.create(schedule=instance, status=False, student=student)
