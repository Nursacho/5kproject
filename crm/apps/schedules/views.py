from rest_framework import viewsets
from apps.schedules.serializers import ScheduleSerializer
from apps.schedules.models import Schedule


class ScheduleAPIViewSet(viewsets.ModelViewSet):
    queryset = Schedule.objects.prefetch_related('schedule_attendances').all()
    serializer_class = ScheduleSerializer
