from rest_framework.routers import DefaultRouter
from apps.homeworks import views

router = DefaultRouter()
router.register('homework', views.HomeworkAPIViewSet, basename='homeworks_obj')
router.register('answer', views.StudentAnswerAPIViewSet, basename='student_answers')
router.register('check', views.CheckHomeworkAPIViewSet, basename='check_homeworks')

urlpatterns = router.urls
