from rest_framework import viewsets
from apps.homeworks.serializers import (
    HomeworkSerializer,
    StudentAnswerSerializer,
    CheckHomeworkSerializer,
    StudentAnswerListSerializer,
)
from apps.homeworks.models import (
    Homework,
    StudentAnswer,
    CheckHomework,
)


class HomeworkAPIViewSet(viewsets.ModelViewSet):
    queryset = Homework.objects.all()
    serializer_class = HomeworkSerializer


class StudentAnswerAPIViewSet(viewsets.ModelViewSet):
    queryset = StudentAnswer.objects.all()
    serializer_class = StudentAnswerSerializer

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return StudentAnswerListSerializer
        return self.serializer_class


class CheckHomeworkAPIViewSet(viewsets.ModelViewSet):
    queryset = CheckHomework.objects.all()
    serializer_class = CheckHomeworkSerializer
