from django.contrib import admin
from apps.homeworks.models import Homework, StudentAnswer, CheckHomework


admin.site.register(Homework)
admin.site.register(StudentAnswer)
admin.site.register(CheckHomework)
