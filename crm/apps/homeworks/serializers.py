from rest_framework import serializers
from apps.homeworks.models import (
    Homework,
    StudentAnswer,
    CheckHomework,
)


class HomeworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Homework
        fields = "__all__"


class CheckHomeworkSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckHomework
        fields = "__all__"


class StudentAnswerSerializer(serializers.ModelSerializer):

    class Meta:
        model = StudentAnswer
        fields = "__all__"


class StudentAnswerListSerializer(serializers.ModelSerializer):
    student_check = CheckHomeworkSerializer(many=True, read_only=True)
    student_answer = HomeworkSerializer(many=True, read_only=True)

    class Meta:
        model = StudentAnswer
        fields = "__all__"
