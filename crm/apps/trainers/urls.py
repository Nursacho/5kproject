from apps.trainers import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("", views.TrainerViewSet, basename="trainers")

urlpatterns = router.urls
