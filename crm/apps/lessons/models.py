from django.db import models
from apps.courses.models import Course
from utils.generators import generate_document_filename


class Lesson(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name='Название урока',
        db_index=True,
        blank=True, null=True,
    )
    description = models.TextField(
        verbose_name='Описание урока'
    )
    lesson_video = models.URLField(
        verbose_name='Ссылкой на видео (YouTube)'
    )
    lesson_lecture = models.FileField(
        upload_to=generate_document_filename,
        verbose_name='Лекция урока PDF',
        blank=True, null=True,
    )
    course = models.ForeignKey(
        Course,
        on_delete=models.SET_NULL,
        blank=True, null=True,
        related_name='lessons',
        verbose_name='Курс'
    )

    def __str__(self):
        return f'{self.title} -- {self.course.title}'

    class Meta:
        verbose_name = "ЛЕКЦИЯ"
        verbose_name_plural = "ЛЕКЦИЙ"
        ordering = ("-id",)
