from rest_framework import viewsets
from apps.lessons.serializers import LessonSerializer
from apps.lessons.models import Lesson


class LessonViewSetAPI(viewsets.ModelViewSet):
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer
