from apps.administrators import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("", views.AdministratorViewSet, basename="administrators")

urlpatterns = router.urls
