from apps.administrators.models import Administrator
from apps.users.serializers import UserSerializer
from rest_framework import serializers


class AdministratorSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Administrator
        fields = "__all__"
