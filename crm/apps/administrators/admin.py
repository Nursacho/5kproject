from apps.administrators.models import Administrator
from django.contrib import admin


admin.site.register(Administrator)
