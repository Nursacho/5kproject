from rest_framework.routers import DefaultRouter

from apps.exams import views


router = DefaultRouter()
router.register('exams', views.ExamAPIViewSet, basename='exams')
router.register('user-exams', views.UserExamAPIViewset, basename='user-exams')
router.register('questions', views.QuestionView, basename='questions')


urlpatterns = router.urls
