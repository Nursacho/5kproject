from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.response import Response
from django.db.models import Prefetch

from apps.exams.serializers import (
    ExamSerializer,
    UserExamSerializer,
    ExamDetailSerializer,
    UserExamDetailSerializer,
)

from apps.exams.models import (
    Exam,
    UserExam,
    Question,
    QuestionAnswer,
    UserAnswer,
)


class ExamAPIViewSet(viewsets.ModelViewSet):
    queryset = Exam.objects.prefetch_related(
        Prefetch(
            'questions', queryset=Question.objects.order_by('-id')
        ),
        Prefetch(
            'questions__answers', queryset=QuestionAnswer.objects.order_by('-id')
        )
    ).select_related('course').all().order_by('-id')
    serializer_class = ExamSerializer

    def retrieve(self, request, pk=None):
        instance = Exam.objects.prefetch_related(
            Prefetch(
                'questions', queryset=Question.objects.order_by('-id')
            ),
            Prefetch(
                'questions__answers', queryset=QuestionAnswer.objects.order_by('-id')
            ),
            'user_exams', 'user_exams__user'
        ).select_related('course').get(pk=pk)
        serializer = ExamDetailSerializer(instance)
        return Response(serializer.data)


class UserExamAPIViewset(viewsets.ModelViewSet):
    queryset = UserExam.objects.prefetch_related(
        Prefetch(
            'user_answers', queryset=UserAnswer.objects.order_by('-id')
        ),
        Prefetch(
            'user_answers__question__answers',
            queryset=QuestionAnswer.objects.order_by('-id')
        ),
        'user_answers__check_boxes__answer',
        'user_answers__answer',
        'user_answers__question'
    ).all().order_by('-id')
    serializer_class = UserExamSerializer

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return UserExamDetailSerializer
        return UserExamSerializer


class QuestionView(mixins.DestroyModelMixin,
                   viewsets.GenericViewSet):
    queryset = Question.objects.all()
