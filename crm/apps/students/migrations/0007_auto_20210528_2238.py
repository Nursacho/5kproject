# Generated by Django 3.2 on 2021-05-28 22:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0006_alter_student_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='english_level',
            field=models.CharField(blank=True, choices=[('Beginner', 'Beginner'), ('Elementary', 'Elementary'), ('Intermediate', 'Intermediate'), ('Advanced', 'Advanced')], max_length=100, null=True, verbose_name='Уровень английского'),
        ),
        migrations.AlterField(
            model_name='student',
            name='status',
            field=models.CharField(choices=[('Активный', 'Активный'), ('Ушёл', 'Ушёл'), ('Окончил', 'Окончил')], default='Активный', max_length=255, verbose_name='Текущий статус'),
        ),
    ]
