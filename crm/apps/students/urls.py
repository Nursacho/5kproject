from apps.students import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("", views.StudentViewSet, basename="students")
router.register("export/", views.ExportRenderStudent, basename='export_students')


urlpatterns = router.urls
