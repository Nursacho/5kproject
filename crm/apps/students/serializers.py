import datetime

from apps.students import choices
from apps.students.models import Student
from apps.users.serializers import UserSerializer
from rest_framework import serializers


class StudentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Student
        fields = "__all__"
        read_only_fields = ("student_id", "user")

    def update(self, instance, validated_data):
        data = validated_data.copy()
        status = data.get("status")
        if status:
            if status == choices.LEFT:
                data["finish_date"] = datetime.date.today()
        return super(StudentSerializer, self).update(instance, data)


class StudentDetailSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Student
        fields = "__all__"
        read_only_fields = ("student_id", "user")


class ExportStudentData(serializers.ModelSerializer):
    course = serializers.StringRelatedField()

    class Meta:
        model = Student
        fields = '__all__'
